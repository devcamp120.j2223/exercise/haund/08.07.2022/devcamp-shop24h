import { useEffect } from "react";

function App() {
  const callSever = async(url = {}) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }

  useEffect(() =>{
    callSever("http://localhost:8000/products")
    .then((data) =>{
        console.log(data);
        
    })
    .catch((error) =>{
        console.log(error.message);
    })
},[]);

  return (
    <div >
     
    </div>
  );
}

export default App;
